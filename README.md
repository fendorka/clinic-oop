# OOP Clinic Project

OOP Clinic Project is project, which present OOP connection between Apex classes and represents a scheme of a medical clinic from the point of view of a patient who came to it, signed up and passed the necessary examinations and doctors, and paid the bill received.

## Installation

Deploy classes to your Salesforce org and use the user, which have access to Apex

## Usage

```java
Main.Main('ExampleClinicName');
```

## Debug execution example

```
New clinic ExampleClinicName was created
Created Doctor with the name Dr. House and specialization: Therapist
Created Doctor with the name Dr. Kelso and specialization: Dentist
Created Administrator with the name John Cena
Created Patient with the name John Doe
Appointment was created for the patient John Doe for 2023-06-07 17:30:00 to the doctor Dr. House
Doctor Dr. House has performed examination for the patient John Doe
Certificate issued with the following results >>>
Diagnosis: Bronchitis
Test Results ---------
Therapist's appointment: Bad
Blood Test: Bad
----------------------
Invoice issued. Total amount due: $400.0
Invoice 400.0 has been paid successfully
```

## UML Classes diagram

![Class diagram](/Class_diagram.png "Class diagram")

## Used patterns

• Singleton to create an instance of the Clinic class so that you can only have one at any time

• Builder to create complex Appointment objects with various parameters, such as examination packages and test details 

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
