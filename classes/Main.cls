/**
 * Created by Roman Stepanov on 02.06.2023.
 */

public with sharing class Main {

    public static void Main(String clinicName) {

        Clinic clinic = initializeTestData(clinicName);

        Administrator administrator = clinic.getCurrentAdministrator();
        Patient patient = administrator.addPatient('John Doe');
        PatientCard ptCard = clinic.addPatientCard(patient);

        administrator.setPackageDiscount(clinic.getPackage('Therapist Package'), 30);
        administrator.assignPackageToPatient(ptCard, 'Therapist Package');

        Datetime datetimeApp = Datetime.newInstance(2023, 6, 7, 10, 30, 0);
        administrator.assignAppointment(datetimeApp, ptCard, clinic.getDoctorByName('Dr. House'));
        administrator.sendToDoctor(ptCard);
        administrator.issueCertificate(ptCard);
        administrator.issueInvoice(ptCard);
    }

    private static Clinic initializeTestData(String clinicName) {
        Clinic.createClinic(clinicName, 'Moskovskaya ulica, d.1');
        Clinic clinic = Clinic.getInstance();

        clinic.addDoctor('Dr. House', 'Therapist');
        clinic.addDoctor('Dr. Kelso', 'Dentist');

        clinic.addAdministrator('John Cena');

        MedicalTest bloodTest = MedicalTest.createTest('Blood Test', 'Analysis', 100);
        MedicalTest xRayTest = MedicalTest.createTest('X-Ray', 'Analysis', 200);
        MedicalTest therapistApp = MedicalTest.createTest('Therapist\'s appointment', 'Survey', 300);
        MedicalTest dentistApp = MedicalTest.createTest('Dentist\'s appointment', 'Survey', 400);

        clinic.addPackage('Therapist Package', new List<MedicalTest>{ therapistApp, bloodTest });
        clinic.addPackage('Dentist Package', new List<MedicalTest>{ dentistApp, xRayTest });

        return clinic;
    }
}