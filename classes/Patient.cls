/**
 * Created by Roman Stepanov on 02.06.2023.
 */

public with sharing class Patient extends Human {

    public Patient(String name) {
        super(name);
    }

    public void payInvoice(Decimal totalCost) {
        System.debug('Invoice ' + totalCost + ' has been paid successfully');
    }

    public override void printInfoDebug() {
        System.debug('Created Patient with the name ' + this.name);
    }
}