/**
 * Created by Roman Stepanov on 05.06.2023.
 */

public abstract class Human {
    public String name;
    protected Date birthday;
    protected String gender;

    public Human(String name, Date birthday, String gender) {
        this.name = name;
        this.birthday = birthday;
        this.gender = gender;
    }

    public Human(String name) {
        this.name = name;
        this.gender = 'unknown';
    }

    public abstract void printInfoDebug();
}