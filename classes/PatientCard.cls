/**
 * Created by Roman Stepanov on 15.06.2023.
 */

public with sharing class PatientCard {

    private ExaminationPackage selectedPackage;
    private String diagnosis;
    private Map<String, String> testResults;
    private final List<Appointment> appointments;
    private final Patient patient;

    public PatientCard(Patient patient) {
        this.patient = patient;
        this.appointments = new List<Appointment>();
        this.testResults = new Map<String, String>();
    }

    public Patient getPatient() {
        return this.patient;
    }

    public void setCurrentPackage(ExaminationPackage examPackage) {
        this.selectedPackage = examPackage;
    }

    public ExaminationPackage getCurrentPackage() {
        return this.selectedPackage;
    }

    public void addAppointment(Appointment appointment) {
        this.appointments.add(appointment);
    }

    public Appointment getClosestAppointment() {
        return this.appointments[0];
    }

    public void setDiagnosisAndResults(Map<String, String> testResults, String diagnosis) {
        this.testResults = testResults;
        this.diagnosis = diagnosis;
    }

    public String getDiagnosis() {
        return this.diagnosis;
    }

    public Map<String, String> getTestResults() {
        return this.testResults;
    }
}