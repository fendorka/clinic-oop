/**
 * Created by Roman Stepanov on 02.06.2023.
 */

public with sharing class Doctor extends Human {

    private final String specialization;
    private final static List<String> illnesses = new List<String>{'Viral disease', 'Bacterial disease', 'Bronchitis', 'Diabetes', 'Caries'};

    public Doctor(String name, Date birthday, String gender, String specialization) {
        super(name, birthday, gender);
        this.specialization = specialization;
    }

    public Doctor(String name, String specialization) {
        super(name);
        this.specialization = specialization;
    }

    public void examinePatient(PatientCard ptCard) {
        System.debug('Doctor ' + name + ' has performed examination for the patient ' + ptCard.getPatient().name);
        Boolean isIll = false;
        Map<String, String> testResults = new Map<String, String>();
        List<MedicalTest> tests = ptCard.getCurrentPackage().getExaminations();
        for (MedicalTest test : tests) {
            String result = this.performTest();
            if (result == 'Bad') {
                isIll = true;
            }
            testResults.put(test.getTestName(), result);
        }
        String diagnosis = isIll ? illnesses[(Integer)Math.floor(Math.random() * 5)] : 'Healthy';
        ptCard.setDiagnosisAndResults(testResults, diagnosis);
    }

    public String performTest() {
        Integer random_int = (Integer)Math.floor(Math.random() * 100);
        Boolean isIll = Math.mod(random_int, 2) == 0;
        return isIll ? 'Bad' : 'Good';
    }

    public override void printInfoDebug() {
        System.debug('Created Doctor with the name ' + name + ' and specialization: ' + specialization);
    }
}