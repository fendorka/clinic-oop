/**
 * Created by Roman Stepanov on 08.06.2023.
 */

public with sharing class ExaminationPackage {
    private final String name;
    private Double price;
    private List<MedicalTest> examinations;
    private Double discountPercent;

    public ExaminationPackage(String name, List<MedicalTest> tests) {
        this.name = name;
        this.price = 0;
        if (tests.size() > 0) {
            this.examinations = tests;
        } else {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You can not create Examination Package without tests');
            ApexPages.addMessage(myMsg);
            System.debug('ERROR: You can not create Examination Package without tests');
        }
    }

    public void addTests(List<MedicalTest> tests) {
        for (MedicalTest test : tests) {
            examinations.add(test);
            price += test.getTestPrice();
        }
    }

    public Double getPackagePrice() {
        return discountPercent != null && discountPercent > 0 ? price*(discountPercent/100) : price;
    }

    public List<MedicalTest> getExaminations() {
        return examinations;
    }

    public void setDiscountPercent(Double discountPercent) {
        this.discountPercent = discountPercent;
    }
}