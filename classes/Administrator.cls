/**
 * Created by Roman Stepanov on 02.06.2023.
 */

public with sharing class Administrator extends Human {

    public Administrator(String name, Date birthday, String gender) {
        super(name, birthday, gender);
    }

    public Administrator(String name) {
        super(name);
    }

    public Patient addPatient(String patientName) {
        Clinic clinic = Clinic.getInstance();
        return clinic.addPatient(patientName);
    }

    public void assignPackageToPatient(PatientCard ptCard, String packageName) {
        Clinic clinic = Clinic.getInstance();
        ExaminationPackage examPackage = clinic.getPackage(packageName);
        ptCard.setCurrentPackage(examPackage);
    }

    public void assignAppointment(Datetime datetimeApp, PatientCard ptCard, Doctor doctor) {
        Appointment newAppointment = new Appointment.AppointmentBuilder()
            .withPatientCard(ptCard)
            .withDoctor(doctor)
            .withCreatedBy(name)
            .withAppointmentDate(datetimeApp)
            .build();
        ptCard.addAppointment(newAppointment);
    }

    public void sendToDoctor(PatientCard ptCard) {
        Appointment appointment = ptCard.getClosestAppointment();
        appointment.getDoctor().examinePatient(ptCard);
    }

    public void setPackageDiscount(ExaminationPackage exPackage, Double discountPercent) {
        exPackage.setDiscountPercent(discountPercent);
    }

    public void issueCertificate(PatientCard ptCard) {
        CertificateIssuer.issueCertificate(ptCard);
    }

    public void issueInvoice(PatientCard ptCard) {
        InvoiceIssuer.issueInvoice(ptCard);
    }

    public override void printInfoDebug() {
        System.debug('Created Administrator with the name ' + name);
    }
}