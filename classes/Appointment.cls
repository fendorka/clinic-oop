/**
 * Created by Roman Stepanov on 02.06.2023.
 */

public with sharing class Appointment {
    private Datetime dateAndTime;
    private PatientCard patientCard;
    private Doctor doctor;
    private ExaminationPackage examinationPackage;
    private Double price;
    private String createdBy; //name of Administrator, who created Appointment

    private Appointment() {
    }

    public class AppointmentBuilder {
        private PatientCard ptCard;
        private Doctor doctor;
        private String createdBy;
        private Datetime appointmentDate;

        public AppointmentBuilder withPatientCard(PatientCard ptCard) {
            this.ptCard = ptCard;
            return this;
        }

        public AppointmentBuilder withDoctor(Doctor doctor) {
            this.doctor = doctor;
            return this;
        }

        public AppointmentBuilder withCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public AppointmentBuilder withAppointmentDate(Datetime appointmentDate) {
            this.appointmentDate = appointmentDate;
            return this;
        }

        public Appointment build() {
            if (this.ptCard == null || this.doctor == null || this.appointmentDate == null) {
                throw new IllegalArgumentException('Patient, doctor, and appointment date are required to create an appointment.');
            }

            Appointment appointment = new Appointment();
            appointment.patientCard = this.ptCard;
            appointment.doctor = this.doctor;
            appointment.dateAndTime = this.appointmentDate;
            appointment.createdBy = this.createdBy;

            appointment.examinationPackage = this.ptCard.getCurrentPackage();
            if (appointment.examinationPackage != null) {
                appointment.price = appointment.examinationPackage.getPackagePrice();
            }

            appointment.printInfoDebug();
            return appointment;
        }
    }

    public Doctor getDoctor() {
        return this.doctor;
    }

    public void printInfoDebug() {
        System.debug('Appointment was created for the patient ' + patientCard.getPatient().name + ' for ' + this.dateAndTime + ' to the doctor ' + this.doctor.name);
    }
}