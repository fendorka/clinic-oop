/**
 * Created by Roman Stepanov on 09.09.2023.
 */

public with sharing class InvoiceIssuer {
    public static void issueInvoice(PatientCard ptCard) {
        Decimal totalCost = ptCard.getCurrentPackage().getPackagePrice();
        System.debug('Invoice issued. Total amount due: $' + totalCost);
        ptCard.getPatient().payInvoice(totalCost);
    }
}