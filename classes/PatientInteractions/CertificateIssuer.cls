/**
 * Created by Roman Stepanov on 09.09.2023.
 */

public with sharing class CertificateIssuer {
    public static void issueCertificate(PatientCard ptCard) {
        System.debug('Certificate issued with the following results >>>');
        System.debug('Diagnosis: ' + ptCard.getDiagnosis());
        System.debug('Test Results ---------');
        Map<String, String> getTestResults = ptCard.getTestResults();
        for (String testName : getTestResults.keySet()) {
            System.debug(testName + ': ' + getTestResults.get(testName));
        }
        System.debug('----------------------');
    }
}