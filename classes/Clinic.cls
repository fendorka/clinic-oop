/**
 * Created by Roman Stepanov on 02.06.2023.
 */

public with sharing class Clinic {

    private static Clinic instance;

    private final String name;
    private final String address;
    private final Time startTime;
    private final Time endTime;

    private final Map<String, PatientCard> patientsCardMap;
    private final Map<String, List<Doctor>> doctorsSpecializationMap;
    private final Map<String, Doctor> doctorsNamesMap;
    private final Map<String, Administrator> administratorsNamesMap;
    private final Map<String, Patient> patientsNamesMap;
    private final Map<String, ExaminationPackage> packagesNamesMap;

    private Clinic(String name, String address, Time startTime, Time endTime) {
        this.name = name;
        this.address = address;
        this.startTime = startTime;
        this.endTime = endTime;
        this.patientsCardMap = new Map<String, PatientCard>();
        this.doctorsNamesMap = new Map<String, Doctor>();
        this.doctorsSpecializationMap = new Map<String, List<Doctor>>();
        this.administratorsNamesMap = new Map<String, Administrator>();
        this.patientsNamesMap = new Map<String, Patient>();
        this.packagesNamesMap = new Map<String, ExaminationPackage>();
        System.debug('New clinic ' + name + ' was created');
    }

    public static Clinic getInstance() {
        return instance;
    }

    public static void createClinic(String name, String address, Time startTime, Time endTime) {
        instance = new Clinic(name, address, startTime, endTime);
    }

    public static void createClinic(String name, String address) {
        Time startTime = Time.newInstance(8, 0, 0, 0);
        Time endTime = Time.newInstance(20, 0, 0, 0);
        createClinic(name, address, startTime, endTime);
    }

    public PatientCard addPatientCard(Patient patient) {
        PatientCard ptCard = new PatientCard(patient);
        patientsCardMap.put(patient.name, ptCard);
        return ptCard;
    }

    public ExaminationPackage addPackage(String name, List<MedicalTest> tests) {
        ExaminationPackage pack = new ExaminationPackage(name, tests);
        this.packagesNamesMap.put(name, pack);
        return pack;
    }

    public Doctor addDoctor(String doctorName, String specialization) {
        Doctor doc = new Doctor(doctorName, specialization);
        this.doctorsNamesMap.put(doctorName, doc);
        List<Doctor> doctorsForSpecialization = this.doctorsSpecializationMap.containsKey(specialization)
            ? this.doctorsSpecializationMap.get(specialization)
            : new List<Doctor>();
        doctorsForSpecialization.add(doc);
        this.doctorsSpecializationMap.put(specialization, doctorsForSpecialization);
        doc.printInfoDebug();
        return doc;
    }

    public Administrator addAdministrator(String admName) {
        Administrator adm = new Administrator(admName);
        this.administratorsNamesMap.put(admName, adm);
        adm.printInfoDebug();
        return adm;
    }

    public Patient addPatient(String patientName) {
        Patient patient = new Patient(patientName);
        this.patientsNamesMap.put(patientName, patient);
        patient.printInfoDebug();
        return patient;
    }

    public void addTestsToPackage(String packageName, List<MedicalTest> tests) {
        if (packagesNamesMap.containsKey(packageName) && tests.size() != 0) {
            packagesNamesMap.get(packageName).addTests(tests);
        }
    }

    public Administrator getCurrentAdministrator() {
        return this.administratorsNamesMap.values()[0];
    }

    public ExaminationPackage getPackage(String packageName) {
        return this.packagesNamesMap.get(packageName);
    }

    public Doctor getDoctorByName(String name) {
        return this.doctorsNamesMap.get(name);
    }

    public List<Doctor> getDoctorsBySpecialization(String specialization) {
        return this.doctorsSpecializationMap.get(specialization);
    }
}