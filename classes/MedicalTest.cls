/**
 * Created by Roman Stepanov on 15.06.2023.
 */

public with sharing class MedicalTest {
    private final String name;
    private final String type;
    private final Double price;

    private MedicalTest(String name, String type, Double price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public static MedicalTest createTest(String name, String type, Double price) {
        return new MedicalTest(name, type, price);
    }

    public Double getTestPrice() {
        return price;
    }

    public String getTestName() {
        return name;
    }
}